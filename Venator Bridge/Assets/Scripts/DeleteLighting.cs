﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteLighting : MonoBehaviour
{
    public Transform controller;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.transform.GetComponent<MainButtonControl>().turnMainOn == true)
        {
            Destroy(this.gameObject);
        }
    }
}
