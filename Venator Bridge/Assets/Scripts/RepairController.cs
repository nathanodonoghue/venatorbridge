﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RepairController : MonoBehaviour
{
    public string repairTriggerName;
    public Transform repairTriggerVolume;
    public Transform sparks;
    public Light Light;
    public bool inRangeRepair;
    public bool fixedRepair;
    public AudioSource repairSound;
    public TextMeshProUGUI repairText;
    public Color newColor;
    public Color lightcolor;

    // Start is called before the first frame update
    void Start()
    {
        inRangeRepair = false;
        fixedRepair = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("f") && inRangeRepair == true)
        {
            Destroy(sparks.gameObject);
            Destroy(repairSound);
            fixedRepair = true;
            repairText.color = newColor;
            Light.color = lightcolor;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == repairTriggerName)
        {
            inRangeRepair = true; // enter the trigger volume
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name == repairTriggerName)
        {
            inRangeRepair = false; // exited the trigger volume
        }
    }
}
