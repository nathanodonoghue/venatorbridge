﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightingController : MonoBehaviour
{
    float intensityLevel; // the current intensity level
    float brightenInc; //how much the brightness increases
    public float startIntensity; //how bright the light is at the start
    public float fullIntensity; //how bright the light will get when increasing after f is pressed
    public Transform controller;

    // Start is called before the first frame update
    void Start()
    {
        brightenInc = fullIntensity / 20;
        this.GetComponent<Light>().intensity = startIntensity;
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.transform.GetComponent<ButtonDownController>().turnOn == true)
        {
            if (intensityLevel < fullIntensity)
            {
                this.GetComponent<Light>().intensity = intensityLevel;
                intensityLevel = intensityLevel + brightenInc;
            }
        }
    }
}
