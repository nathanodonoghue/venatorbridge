﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorController : MonoBehaviour
{
    public string playerName;
    public bool doorRange;

    // Start is called before the first frame update
    void Start()
    {
        doorRange = false;
    }

    // Update is called once per frame
    void Update()
    {
       
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == playerName)
        {
            doorRange = true; // enter the trigger volume
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name == playerName)
        {
            doorRange = false; // enter the trigger volume
        }
    }
}
