﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ButtonDownController : MonoBehaviour
{

    public string triggerName;
    public Transform TriggerVolume;
    public Transform Button;
    Animator animButton;
    public bool inRange;
    public bool turnOn;
    public AudioSource ButtonSound;
    public AudioSource ComputerHum1;
    public TextMeshProUGUI HoloText;
    public Color newColor;


    // Start is called before the first frame update
    void Start()
    {
        animButton = Button.GetComponent<Animator>();
        inRange = false;
        turnOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("f") && inRange == true)
        {
            animButton.SetTrigger("PressButton");
            turnOn = true;
            ButtonSound.Play();
            ComputerHum1.Play();
            HoloText.color = newColor;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == triggerName)
        {
            inRange = true; // enter the trigger volume
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name == triggerName)
        {
            inRange = false; // exited the trigger volume
        }
    }
}
