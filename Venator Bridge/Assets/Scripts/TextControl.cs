﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextControl : MonoBehaviour
{
    public string instruct;
    public string emptyText;
    public string repairtext;
    public TextMeshProUGUI cameraText;
    public Transform Camera;
    public Transform Player;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.transform.GetComponent<MainButtonControl>().inMainRange == true && Player.transform.GetComponent<MainButtonControl>().turnMainOn == false)
        {
            cameraText.text = instruct;
        }
        else if (Player.transform.GetComponent<ButtonDownController>().inRange == true && Player.transform.GetComponent<ButtonDownController>().turnOn == false)
        {
            cameraText.text = instruct;
        }
        else if (Player.transform.GetComponent<RepairController>().inRangeRepair == true && Player.transform.GetComponent<RepairController>().fixedRepair == false)
        {
            cameraText.text = repairtext;
        }
        else
        {
            cameraText.text = emptyText;
        }
    }
}
