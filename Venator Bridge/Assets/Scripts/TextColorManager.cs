﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TextColorManager : MonoBehaviour
{
    public Transform Player;
    public TextMeshProUGUI congratsText;
    public Color newColor;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Player.transform.GetComponent<MainButtonControl>().turnMainOn == true && Player.transform.GetComponent<ButtonDownController>().turnOn == true)
        {
            congratsText.color = newColor;
        }
    }
}
