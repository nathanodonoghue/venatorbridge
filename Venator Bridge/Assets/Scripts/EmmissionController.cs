﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmmissionController : MonoBehaviour
{
    public Material startMat;
    public Material EmmissMat;
    public Transform controller;
    public int randomNum;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Renderer>().material = startMat;
        randomNum = Random.Range(5,30);
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.transform.GetComponent<ButtonDownController>().turnOn == true)
        {
            if (randomNum == 0)
            {
                this.GetComponent<Renderer>().material = EmmissMat;
            }
            else
            {
                randomNum--;
            }
        }
    }
}
