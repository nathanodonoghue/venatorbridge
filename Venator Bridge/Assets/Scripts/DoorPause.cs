﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorPause : MonoBehaviour
{
    public Transform player;
    public Animator animDoor;
    public Transform trigger;
    public AudioSource DoorSound;

    // Start is called before the first frame update
    void Start()
    {
        animDoor = this.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (trigger.transform.GetComponent<DoorController>().doorRange == true && player.transform.GetComponent<ButtonDownController>().turnOn == true)
        {
            animDoor.SetTrigger("DoorOpen");
        }
        if (trigger.transform.GetComponent<DoorController>().doorRange == false && player.transform.GetComponent<ButtonDownController>().turnOn == true)
        {
            animDoor.enabled = true;
            DoorSound.Play();
        }
    }

    public void PauseAnimEvent()
    {
        animDoor.enabled = false;
    }
}
