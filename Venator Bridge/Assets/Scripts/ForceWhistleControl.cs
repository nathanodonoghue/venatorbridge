﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ForceWhistleControl : MonoBehaviour
{
    public AudioSource forceWhistle;
    public AudioSource ForceTheme;
    public Transform controller;
    int timer;
    bool playing;

    // Start is called before the first frame update
    void Start()
    {
        timer = 0;
        playing = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.transform.GetComponent<ButtonDownController>().turnOn == true)
        {
            if (timer == 0)
            {
                forceWhistle.Play();
                timer = 250;
            }
            else if (timer > 0 && playing == false)
            {
                timer--;
            }
        }
        if (controller.transform.GetComponent<MainButtonControl>().turnMainOn == true && playing == false)
        {
            Debug.Log("are you playing");
            ForceTheme.Play();
            playing = true;
        }
    }
}
