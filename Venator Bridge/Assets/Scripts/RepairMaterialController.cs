﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepairMaterialController : MonoBehaviour
{
    public Material startMat;
    public Material EmmissMat;
    public Transform controller;

    // Start is called before the first frame update
    void Start()
    {
        this.GetComponent<Renderer>().material = startMat;
    }

    // Update is called once per frame
    void Update()
    {
        if (controller.transform.GetComponent<RepairController>().fixedRepair == true)
        {
            this.GetComponent<Renderer>().material = EmmissMat;
        }
    }
}
