﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MainButtonControl : MonoBehaviour
{
    public string mainTriggerName;
    public Transform mainTriggerVolume;
    public Transform mainButton;
    Animator animMainButton;
    public bool inMainRange;
    public bool turnMainOn;
    public AudioSource ButtonSound;
    public AudioSource ComputerHum2;
    public TextMeshProUGUI MainText;
    public Color newColor;

    // Start is called before the first frame update
    void Start()
    {
        animMainButton = mainButton.GetComponent<Animator>();
        inMainRange = false;
        turnMainOn = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown("f") && inMainRange == true)
        {
            animMainButton.SetTrigger("PowerOn");
            turnMainOn = true;
            ButtonSound.Play();
            ComputerHum2.Play();
            MainText.color = newColor;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject.name == mainTriggerName)
        {
            inMainRange = true; // enter the trigger volume
        }
    }

    void OnTriggerExit(Collider collider)
    {
        if (collider.gameObject.name == mainTriggerName)
        {
            inMainRange = false; // exited the trigger volume
        }
    }
}
